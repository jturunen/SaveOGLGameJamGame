﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TargetController : MonoBehaviour
{
    public bool recruitable;

    public CharData character;

    public int role;
    public int genre;
    public int skill;

    public AppearanceModel appearance;

    private Vector2 targetVector;

    private float latestDirectionChangeTime;
    private readonly float directionChangeTime = 1f;
    private float characterVelocity = 1f;
    private Vector2 movementDirection;
    private Vector2 movementPerSecond;

    public Sprite ballNormal, ballDisabled;

    void Start()
    {
        activate();
        /**
         * Generator random int (from pool of something to something). This int is the position of the sprite in the atlas
         */
        int hairRng = Random.Range(1, TargetSpriteController.Instance.hairPool.Length + 1);
        int browsRng = Random.Range(1, TargetSpriteController.Instance.browsPool.Length + 1);
        int eyesRng = Random.Range(1, TargetSpriteController.Instance.eyesPool.Length + 1);
        int noseRng = Random.Range(1, TargetSpriteController.Instance.nosePool.Length + 1);
        int mouthRng = Random.Range(1, TargetSpriteController.Instance.mouthPool.Length + 1);
        int bodyRng = Random.Range(1, TargetSpriteController.Instance.bodyPool.Length + 1);

        appearance = new AppearanceModel(
        TargetSpriteController.Instance.hairPool.Single(s => s.name == "Hair" + hairRng),
        TargetSpriteController.Instance.browsPool.Single(s => s.name == "Brows" + browsRng),
        TargetSpriteController.Instance.eyesPool.Single(s => s.name == "Eyes" + eyesRng),
        TargetSpriteController.Instance.nosePool.Single(s => s.name == "Nose" + noseRng),
        TargetSpriteController.Instance.mouthPool.Single(s => s.name == "Mouth" + mouthRng),
        TargetSpriteController.Instance.bodyPool.Single(s => s.name == "Body" + bodyRng));

        latestDirectionChangeTime = 0f;
        calcuateNewMovementVector();
    }

    // Update is called once per frame
    void Update()
    {
        if (recruitable)
        {
            //if the changeTime was reached, calculate a new movement vector
            if (Time.time - latestDirectionChangeTime > directionChangeTime)
            {
                latestDirectionChangeTime = Time.time;
                calcuateNewMovementVector();
            }

            //move enemy: 
            transform.position = new Vector2(transform.position.x + (movementPerSecond.x * Time.deltaTime),
            transform.position.y + (movementPerSecond.y * Time.deltaTime));
        }
    }

    void calcuateNewMovementVector()
    {
        //create a random direction vector with the magnitude of 1, later multiply it with the velocity of the enemy
        movementDirection = new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f)).normalized;
        movementPerSecond = movementDirection * characterVelocity;
    }

    public void activate()
    {
        gameObject.GetComponent<CapsuleCollider>().enabled = true;
        recruitable = true;
        GetComponent<SpriteRenderer>().sprite = ballNormal;
    }

    public void deactivate()
    {
        if (recruitable)
        {
            gameObject.GetComponent<CapsuleCollider>().enabled = false;
            recruitable = false;
            GetComponent<SpriteRenderer>().sprite = ballDisabled;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.name == "TopWall")
        {
            movementDirection = new Vector2(-1, -1).normalized;
            movementPerSecond = movementDirection * characterVelocity;
        }
        if (other.name == "LeftWall")
        {
            movementDirection = new Vector2(1, 1).normalized;
            movementPerSecond = movementDirection * characterVelocity;
        }
        if (other.name == "RightWall")
        {
            movementDirection = new Vector2(-1, -1).normalized;
            movementPerSecond = movementDirection * characterVelocity;
        }
        if (other.name == "BottomWall")
        {
            movementDirection = new Vector2(1, 1).normalized;
            movementPerSecond = movementDirection * characterVelocity;
        }
        

    }
}
