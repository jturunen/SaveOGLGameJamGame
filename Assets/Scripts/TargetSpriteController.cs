﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetSpriteController : MonoBehaviour
{
    public static TargetSpriteController Instance = null;
    public Sprite[] hairPool;
    public Sprite[] browsPool;
    public Sprite[] eyesPool;
    public Sprite[] nosePool;
    public Sprite[] mouthPool;
    public Sprite[] bodyPool;
    // Start is called before the first frame update
    void Start()
    {

        if (Instance == null)
        {
            Instance = this;
        }

        hairPool = Resources.LoadAll<Sprite>("Sprites/Hair/");
        browsPool = Resources.LoadAll<Sprite>("Sprites/Brows/");
        eyesPool = Resources.LoadAll<Sprite>("Sprites/Eyes/");
        nosePool = Resources.LoadAll<Sprite>("Sprites/Nose/");
        mouthPool = Resources.LoadAll<Sprite>("Sprites/Mouth/");
        bodyPool = Resources.LoadAll<Sprite>("Sprites/Body/");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
