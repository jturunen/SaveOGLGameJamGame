﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerController : MonoBehaviour
{
    public GameController gameController;
    public GameObject target;

    private float maxX = 7;
    private float minX = -7;
    private float maxY = 3f;
    private float minY = -3f;

    public int minRole = 1;
    public int maxRole = 3;

    public int minGenre = 1;
    public int maxGenre = 6;

    public int minSkill = 1;
    public int maxSkill = 5;
    public int characterInField = 20;
    private int audioCounter = 0, adultGameCounter = 0;

    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i <= characterInField; i++) 
        {
            createTarget();
        }

        gameController.targetArray = GameObject.FindGameObjectsWithTag("Target");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void createTarget()
    {
        GameObject newTarget = Instantiate(target);
        newTarget.transform.position = new Vector3(Random.Range(minX, maxX), Random.Range(maxY, minY), 0f);

        //Character data
        CharData character = new CharData(roleRng(), genreRng(), skillRng());
        newTarget.GetComponent<TargetController>().character = character;

        newTarget.name = "Dev " + character.role + character.genre + character.skill;
    }

    public int roleRng()
    {
        int roleRng = 0;

        roleRng = Random.Range(0, 100);
        

        if (roleRng < 90)
        {
            return Random.Range(minRole, maxRole);
        } else
        {
            return AttributeLibrary.AUDIO;
            
        }
    }

    public int genreRng()
    {
        return Random.Range(minGenre, maxGenre);

        /*int genreRng = 0;


        genreRng = Random.Range(0, 96);
        
        if (genreRng < 96)
        {
            return Random.Range(minGenre, maxGenre);
        }
        else
        {
            return AttributeLibrary.ADULT;
        }*/
    }
    public int skillRng()
    {
        int roleRng = Random.Range(0, 100);

        if (roleRng > 0 && roleRng < 5)
        {
            return 1;
        }
        else if(roleRng > 5 && roleRng < 20)
        {
            return 2;
        }
        else if(roleRng > 20 && roleRng < 55)
        {
            return 3;
        }
        else if(roleRng > 55 && roleRng < 85)
        {
            return 4;
        }
        else if(roleRng > 85 && roleRng < 100)
        {
            return 5;
        } else
        {
            return 3;
        }
    }
}
