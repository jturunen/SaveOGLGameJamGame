﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TalkModeScript : MonoBehaviour
{
    public float targetTime = 0.0f;
    public float timer = 0.0f;
    public Slider slider;

    public TextMeshProUGUI roleText;
    public TextMeshProUGUI genreText;
    public TextMeshProUGUI skillText;
    public Image genreImage;
    public Image skillImage;

    public Image roleBackground;
    public Image genreBackground;
    public Image skillBackground;
    //Sprite[] uiAtlas;
    public Sprite background;
    public Sprite darkBackground;

    public bool talkOpen;
    void Start()
    {
        /*uiAtlas = Resources.LoadAll<Sprite>("Sprites/UI/ui_atlas");
        campSprite = uiAtlas.Single(s => s.name == "icon_atlas_2");*/
        genreImage.gameObject.SetActive(false);
        skillImage.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public IEnumerator startTalkTimer(GameController gameController, CharData character)
    {
        talkOpen = true;
        while (timer <= 5f && talkOpen)
        {
            yield return null;
            timer+= 0.01666f;
            slider.value = timer;

            if(timer >= 1f)
            {
                roleText.text = AttributeLibrary.roleToString(character.role);
                roleBackground.sprite = background;
            }
            if (timer >= 2f)
            {
                genreText.text = ""; // AttributeLibrary.genreToString(character.genre);
                genreImage.sprite = AttributeLibrary.genreToSprite(character.genre);
                genreImage.gameObject.SetActive(true);
                genreBackground.sprite = background;
            }
            if (timer >= 3f)
            {
                skillText.text = ""; // AttributeLibrary.skillToString(character.skill);
                skillImage.sprite = AttributeLibrary.skillToSprite(character.skill);
                skillImage.gameObject.SetActive(true);
                skillBackground.sprite = background;
            }
        }

        gameController.closeTalkMode();

    }

    public void resetUI()
    {
        talkOpen = false;
        skillBackground.sprite = darkBackground;
        genreBackground.sprite = darkBackground;
        roleBackground.sprite = darkBackground;
        targetTime = 0.0f;
        timer = 0.0f;
        slider.value = 0;
        roleText.text = "???";
        genreText.text ="???";
        skillText.text = "???";
        genreImage.gameObject.SetActive(false);
        skillImage.gameObject.SetActive(false);

    }
}
