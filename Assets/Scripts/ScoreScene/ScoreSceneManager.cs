﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScoreSceneManager : MonoBehaviour
{
    public GameObject namingPanel;
    public GameObject scoringPanel;
    public GameObject highscorePanel;
    
    // Start is called before the first frame update
    void Start()
    {
        openNamingPanel();
        AudioManager.instance.PlayLoopMusic(AudioManager.instance.peace);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void clearMembers()
    {
        PlayerPrefs.DeleteKey(PlayerPrefKeys.CURRENT_GAME_NAME_KEY);
        PlayerPrefs.DeleteKey(PlayerPrefKeys.CURRENT_GAME_SCORE_KEY);
        PlayerPrefs.DeleteKey(PlayerPrefKeys.NUMBER_OF_MEMBERS);

        for (int i = 0; i < 4; i++)
        {
            string memberOneRole = "MEMBER_ROLE_" + i;
            string memberOneGenre = "MEMBER_GENRE_" + i;
            string memberOneSkill = "MEMBER_SKILL_" + i;

            PlayerPrefs.DeleteKey(memberOneRole);
            PlayerPrefs.DeleteKey(memberOneGenre);
            PlayerPrefs.DeleteKey(memberOneSkill);
        }
    }

    public void restartGame()
    {
        clearMembers();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

    public void returnToMenu()
    {
        clearMembers();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 2);
    }

    public void openNamingPanel()
    {
        namingPanel.SetActive(true);
        scoringPanel.SetActive(false);
        highscorePanel.SetActive(false);
    }

    public void openScoringPanel()
    {
        namingPanel.SetActive(false);
        scoringPanel.SetActive(true);
        highscorePanel.SetActive(false);
    }

    public void openHighscorePanel()
    {
        namingPanel.SetActive(false);
        scoringPanel.SetActive(false);
        highscorePanel.SetActive(true);
    }
}
