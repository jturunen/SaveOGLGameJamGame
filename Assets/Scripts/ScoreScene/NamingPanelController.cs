﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;

public class NamingPanelController : MonoBehaviour
{
    public TMP_InputField inputField;
    public GameObject scoringPanel;
    
    // Start is called before the first frame update
    void Start()
    {
        EventSystem.current.SetSelectedGameObject(inputField.gameObject, null);
    }

    void Update()
    {
        if (!inputField.isFocused)
        {
            EventSystem.current.SetSelectedGameObject(inputField.gameObject, null);
        }
    }

    public void handleSubmit()
    {
        string gameName = inputField.text.ToString() == "" ? "NoNameGame" : inputField.text.ToString();
        PlayerPrefs.SetString(PlayerPrefKeys.CURRENT_GAME_NAME_KEY, gameName);
    }
}
