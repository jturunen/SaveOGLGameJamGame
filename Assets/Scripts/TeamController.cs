﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CharData
{
    public int role { get; set; }
    public int genre { get; set; }
    public int skill { get; set; }

    public CharData(int role, int genre, int skill)
    {
        this.role = role;
        this.genre = genre;
        this.skill = skill;
    }
}


public class TeamController : MonoBehaviour
{
    public GameObject PersonCard1;
    public GameObject PersonCard2;
    public GameObject PersonCard3;
    public GameObject PersonCard4;
    public GameObject PersonCard5;

    public Image Person1GenreImage;
    public Image Person2GenreImage;
    public Image Person3GenreImage;
    public Image Person4GenreImage;
    public Image Person5GenreImage;

    public TextMeshProUGUI Person1RoleText;
    public TextMeshProUGUI Person2RoleText;
    public TextMeshProUGUI Person3RoleText;
    public TextMeshProUGUI Person4RoleText;
    public TextMeshProUGUI Person5RoleText;

    public Image Person1SkillImage;
    public Image Person2SkillImage;
    public Image Person3SkillImage;
    public Image Person4SkillImage;
    public Image Person5SkillImage;

    [SerializeField]
    private int personCounter = 0;
    public const int MAX_TEAM_MEMBERS = 5;

    public List<CharData> team = new List<CharData>();
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void addToTeam(CharData character)
    {
        int index = team.Count;
        team.Add(character);
        personCounter++;
        setPersonCard(character);
        PlayerPrefs.SetInt(PlayerPrefKeys.NUMBER_OF_MEMBERS, index + 1);

        string roleKey = "MEMBER_ROLE_" + index;
        string genreKey = "MEMBER_GENRE_" + index;
        string skillKey = "MEMBER_SKILL_" + index;
        
        PlayerPrefs.SetInt(roleKey, character.role);
        PlayerPrefs.SetInt(genreKey, character.genre);
        PlayerPrefs.SetInt(skillKey, character.skill);

        //If full, stop the game.
        if (team.Count == MAX_TEAM_MEMBERS)
        {
            GameObject.FindObjectOfType<GameController>().endGame();
        }
    }

    public void setPersonCard(CharData character)
    {
        Debug.Log("Hello");
        switch (personCounter)
        {
            case 1:
                {
                    PersonCard1.SetActive(true);
                    Person1GenreImage.sprite = AttributeLibrary.genreToSprite(character.genre);
                    Person1SkillImage.sprite = AttributeLibrary.skillToSprite(character.skill);
                    Person1RoleText.text = AttributeLibrary.roleToString(character.role);
                    break;
                }
            case 2:
                {
                    PersonCard2.SetActive(true);
                    Person2GenreImage.sprite = AttributeLibrary.genreToSprite(character.genre);
                    Person2SkillImage.sprite = AttributeLibrary.skillToSprite(character.skill);
                    Person2RoleText.text = AttributeLibrary.roleToString(character.role);
                    break;
                }
            case 3:
                {
                    PersonCard3.SetActive(true);
                    Person3GenreImage.sprite = AttributeLibrary.genreToSprite(character.genre);
                    Person3SkillImage.sprite = AttributeLibrary.skillToSprite(character.skill);
                    Person3RoleText.text = AttributeLibrary.roleToString(character.role);
                    break;
                }
            case 4:
                {
                    PersonCard4.SetActive(true);
                    Person4GenreImage.sprite = AttributeLibrary.genreToSprite(character.genre);
                    Person4SkillImage.sprite = AttributeLibrary.skillToSprite(character.skill);
                    Person4RoleText.text = AttributeLibrary.roleToString(character.role);
                    break;
                }
            case 5:
                {
                    PersonCard5.SetActive(true);
                    Person5GenreImage.sprite = AttributeLibrary.genreToSprite(character.genre);
                    Person5SkillImage.sprite = AttributeLibrary.skillToSprite(character.skill);
                    Person5RoleText.text = AttributeLibrary.roleToString(character.role);
                    break;
                }
            default:
                {
                    break;
                }
        }
    }
}
