﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class AttributeLibrary
{
    public const int CODER = 1;
    public const int ARTIST = 2;
    public const int DESIGNER = 3;
    public const int AUDIO = 4;

    public const int RPG = 1;
    public const int ACTION = 2;
    public const int STRATEGY = 3;
    public const int PUZZLE = 4;
    public const int ADULT = 5;

    public const int ONESTAR = 1;
    public const int TWOSTAR = 2;
    public const int THREESTAR = 3;
    public const int FOURSTAR = 4;
    public const int FIVESTAR = 5;
    private static Sprite[] uiAtlas;

    public static string roleToString(int role)
    {
        switch (role)
        {
            case CODER:
                {
                    return "Programmer";
                }
            case ARTIST:
                {
                    return "Artist";
                }
            case DESIGNER:
                {
                    return "Designer";
                }
            case AUDIO:
                {
                    return "Audio";
                }
            default:
                {
                    return "Defaulted???";
                }
        }
    }

    public static string genreToString(int role)
    {
        switch (role)
        {
            case RPG:
                {
                    return "RPG";
                }
            case ACTION:
                {
                    return "Action";
                }
            case STRATEGY:
                {
                    return "Strategy";
                }
            case PUZZLE:
                {
                    return "Puzzle";
                }
            case ADULT:
                {
                    return "Dating";
                }
            default:
                {
                    return "Defaulted???";
                }
        }
    }
    public static Sprite genreToSprite(int role)
    {
        uiAtlas = Resources.LoadAll<Sprite>("Sprites/UI/ui_atlas");
        switch (role)
        {
            case RPG:
                {
                    return uiAtlas.Single(s => s.name == "ui_atlas_15");
                }
            case ACTION:
                {
                    return uiAtlas.Single(s => s.name == "ui_atlas_16");
                }
            case STRATEGY:
                {
                    return uiAtlas.Single(s => s.name == "ui_atlas_14");
                }
            case PUZZLE:
                {
                    return uiAtlas.Single(s => s.name == "ui_atlas_28");
                }
            case ADULT:
                {
                    return uiAtlas.Single(s => s.name == "ui_atlas_17");
                }
            default:
                {
                    return uiAtlas.Single(s => s.name == "ui_atlas_17");
                }
        }
    }

    public static string skillToString(int role)
    {
        switch (role)
        {
            case ONESTAR:
                {
                    return "One star";
                }
            case TWOSTAR:
                {
                    return "Two star";
                }
            case THREESTAR:
                {
                    return "Three star";
                }
            case FOURSTAR:
                {
                    return "Four star";
                }
            case FIVESTAR:
                {
                    return "Five star";
                }
            default:
                {
                    return "Defaulted???";
                }
        }
    }

    public static Sprite skillToSprite(int role)
    {
        uiAtlas = Resources.LoadAll<Sprite>("Sprites/UI/ui_atlas");
        switch (role)
        {
            case ONESTAR:
                {
                    return uiAtlas.Single(s => s.name == "ui_atlas_10");
                }
            case TWOSTAR:
                {
                    return uiAtlas.Single(s => s.name == "ui_atlas_7");
                }
            case THREESTAR:
                {
                    return uiAtlas.Single(s => s.name == "ui_atlas_2");
                }
            case FOURSTAR:
                {
                    return uiAtlas.Single(s => s.name == "ui_atlas_19");
                }
            case FIVESTAR:
                {
                    return uiAtlas.Single(s => s.name == "ui_atlas_27");
                }
            default:
                {
                    return uiAtlas.Single(s => s.name == "ui_atlas_10");
                }
        }
    }
}
