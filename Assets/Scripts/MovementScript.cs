﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementScript : MonoBehaviour
{
    Rigidbody body;

    float horizontal;
    float vertical;

    public float runSpeed = 20.0f;
    internal bool canMove = true;

    void Start()
    {
        body = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if(canMove)
        {
            horizontal = Input.GetAxisRaw("Horizontal");
            vertical = Input.GetAxisRaw("Vertical");
            if (horizontal != 0 || vertical != 0)
            {
                walkSoundToggle();
            }
            else if (horizontal == 0 || vertical == 0)
            {
                AudioManager.instance.audioSource1.Stop();
            }
        } else
        {
            horizontal = 0;
            vertical = 0;
        }
    }

    public void walkSoundToggle() {
        if (!AudioManager.instance.audioSource1.isPlaying)
        {
            AudioManager.instance.PlayLoop1(AudioManager.instance.walking);
        }
    }


    private void FixedUpdate()
    {
        body.velocity = new Vector2(horizontal * runSpeed, vertical * runSpeed);
    }
}
