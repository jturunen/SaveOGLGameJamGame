﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractScript : MonoBehaviour
{
    public bool entered = true;
    [SerializeField]
    private GameObject target;
    public GameObject bubble;
    public GameController gameController;
    // Start is called before the first frame update
    void Start()
    {
        gameController = GameObject.Find("GameController").GetComponent<GameController>();

    }

    // Update is called once per frame
    void Update()
    {
        if(target != null)
        {
            if (target.GetComponent<TargetController>().recruitable && !gameController.talkView.activeInHierarchy && gameController.gameStarted)
            {
                if(!bubble.activeInHierarchy)
                {
                    bubble.SetActive(true);
                }
                //Debug.Log("Recruitable");
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    Debug.Log("Talk to target" + target.name);
                    TargetController targetController = target.GetComponent<TargetController>();
                    //Launch the talk encounter gamectrl.launc(target.appearance)
                    gameController.launchTalkMode(targetController.appearance, targetController.character);
                    //Set target disabled
                    targetController.deactivate();
                    bubble.SetActive(false);

                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        entered = true;
       /* if (target == null)
        {*/
        if(other.GetComponent<TargetController>().recruitable)
        {
            target = other.gameObject;
        } else
        {
            Debug.Log("not recruitable, lets not select it");
        }
       // } 

    }

    private void OnTriggerStay(Collider other)
    {
        entered = true;
        /* if (target == null)
         {*/
        if (other.GetComponent<TargetController>().recruitable)
        {
            target = other.gameObject;
        }
        else
        {
            Debug.Log("not recruitable, lets not select it");
        }
        // } 

    }

    private void OnTriggerExit(Collider other)
    {
        entered = false;
        target = null;
        bubble.SetActive(false);

    }

}
