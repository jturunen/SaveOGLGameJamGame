﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HighScoreController : MonoBehaviour
{
    public GameObject scoreHolderObject;
    public GameObject scoreObject;

    public class HighScore
    {
        public string name = "-";
        public int score = 0;

        public HighScore(string name, int score)
        {
            this.name = name;
            this.score = score;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        // Show the holder, hide the mock object
        scoreHolderObject.SetActive(true);
        scoreObject.SetActive(false);

        // Get the current score information
        HighScore currentScore = new HighScore(PlayerPrefs.GetString(PlayerPrefKeys.CURRENT_GAME_NAME_KEY), PlayerPrefs.GetInt(PlayerPrefKeys.CURRENT_GAME_SCORE_KEY));

        // Get existing score information
        List<HighScore> highScores = new List<HighScore>();
        for (int i = 0; i < 5; i++)
        {
            string nameKey = "HIGHSCORE_NAME_PLACE_" + i;
            string valueKey = "HIGHSCORE_VALUE_PLACE_" + i;
            //Debug.Log(i);
            highScores.Add(new HighScore(PlayerPrefs.GetString(nameKey), PlayerPrefs.GetInt(valueKey)));
        }
        
        for (int i = 0; i < highScores.Count; i++)
        {
            if (currentScore.score > highScores[i].score)
            {
                highScores.Insert(i, currentScore);         // insert and shift others
                highScores.RemoveAt(highScores.Count-1);    // remove last
                
                for (int j = 0; j < highScores.Count; j++)
                {
                    string nameKey = "HIGHSCORE_NAME_PLACE_" + j;
                    string valueKey = "HIGHSCORE_VALUE_PLACE_" + j;

                    PlayerPrefs.SetString(nameKey, highScores[j].name);
                    PlayerPrefs.SetInt(valueKey, highScores[j].score);
                }

                break; // Break out because we replaced something
            }
        }

        for (int i = 0; i < highScores.Count; i++)
        {
            GameObject singleScore = Instantiate(scoreObject);
            singleScore.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = highScores[i].name;
            singleScore.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = highScores[i].score.ToString();

            singleScore.transform.SetParent(scoreHolderObject.transform);
            singleScore.transform.localPosition = new Vector2(0, 200 + (-100 * i));
            singleScore.SetActive(true);
        }
    }
}
