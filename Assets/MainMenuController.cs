﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    public GameObject howToObject;

    // Start is called before the first frame update
    void Start()
    {
        AudioManager.instance.PlayLoopMusic(AudioManager.instance.crowd);

        howToObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void startGame()
    {
        ScoreSceneManager.clearMembers();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void exitGame()
    {
        Debug.Log("Quit game");
        ScoreSceneManager.clearMembers();
        Application.Quit();
    }

}
