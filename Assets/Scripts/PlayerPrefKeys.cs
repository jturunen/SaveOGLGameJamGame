﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerPrefKeys
{
    public static string CURRENT_GAME_NAME_KEY = "GameNameKey";
    public static string CURRENT_GAME_SCORE_KEY = "GameScoreKey";


    #region HIGHSCORES
    public static string HIGHSCORE_NAME_PLACE_0 = "HIGHSCORE_NAME_PLACE_0";
    public static string HIGHSCORE_NAME_PLACE_1 = "HIGHSCORE_NAME_PLACE_1";
    public static string HIGHSCORE_NAME_PLACE_2 = "HIGHSCORE_NAME_PLACE_2";
    public static string HIGHSCORE_NAME_PLACE_3 = "HIGHSCORE_NAME_PLACE_3";
    public static string HIGHSCORE_NAME_PLACE_4 = "HIGHSCORE_NAME_PLACE_4";

    public static string HIGHSCORE_VALUE_PLACE_0 = "HIGHSCORE_VALUE_PLACE_0";
    public static string HIGHSCORE_VALUE_PLACE_1 = "HIGHSCORE_VALUE_PLACE_1";
    public static string HIGHSCORE_VALUE_PLACE_2 = "HIGHSCORE_VALUE_PLACE_2";
    public static string HIGHSCORE_VALUE_PLACE_3 = "HIGHSCORE_VALUE_PLACE_3";
    public static string HIGHSCORE_VALUE_PLACE_4 = "HIGHSCORE_VALUE_PLACE_4";
    #endregion


    #region TEAM
    public static string NUMBER_OF_MEMBERS = "NUMBER_OF_MEMBERS";

        #region Member 0
        public static string MEMBER_ROLE_0 = "MEMBER_ROLE_0";
        public static string MEMBER_GENRE_0 = "MEMBER_GENRE_0";
        public static string MEMBER_SKILL_0 = "MEMBER_SKILL_0";
        #endregion

        #region Member 1
        public static string MEMBER_ROLE_1 = "MEMBER_ROLE_1";
        public static string MEMBER_GENRE_1 = "MEMBER_GENRE_1";
        public static string MEMBER_SKILL_1 = "MEMBER_SKILL_1";
        #endregion

        #region Member 2
        public static string MEMBER_ROLE_2 = "MEMBER_ROLE_2";
        public static string MEMBER_GENRE_2 = "MEMBER_GENRE_2";
        public static string MEMBER_SKILL_2 = "MEMBER_SKILL_2";
        #endregion

        #region Member 3
        public static string MEMBER_ROLE_3 = "MEMBER_ROLE_3";
        public static string MEMBER_GENRE_3 = "MEMBER_GENRE_3";
        public static string MEMBER_SKILL_3 = "MEMBER_SKILL_3";
        #endregion

        #region Member 4
        public static string MEMBER_ROLE_4 = "MEMBER_ROLE_4";
        public static string MEMBER_GENRE_4 = "MEMBER_GENRE_4";
        public static string MEMBER_SKILL_4 = "MEMBER_SKILL_4";
        #endregion
    #endregion
}
