﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
//AudioManager.instance.PlaySingle(AudioManager.instance.wolfHowl);

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance = null;     //Allows other scripts to call functions from SoundManager.             

    public AudioSource musicSource, audioSource1, audioSource2, audioSource3;
    private float lowPitchRange = 0.5f;
    private float highPitchRange = 2.5f;

    public AudioClip gameIsMade, humm, speech, subtleClick, walking, weirdClick;
    public AudioClip medium, peace, intro, crowd;
    // Use this for initialization
    void Awake()
    {
        //Check if there is already an instance of SoundManager
        if (instance == null)
        {
            //if not, set it to this.
            instance = this;
        }
        //If instance already exists:
        else if (instance != this)
        {
            //Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
            Destroy(gameObject);
        }

        //ambientPool = Resources.LoadAll<AudioClip>("Audio/Animals/Ambient/");
        gameIsMade = Resources.Load<AudioClip>("Audio/GameIsMade");
        humm = Resources.Load<AudioClip>("Audio/Humm");
        speech = Resources.Load<AudioClip>("Audio/Speech");
        subtleClick = Resources.Load<AudioClip>("Audio/SubtleClick");
        walking = Resources.Load<AudioClip>("Audio/Walking");
        weirdClick = Resources.Load<AudioClip>("Audio/WeirdClick");
        medium = Resources.Load<AudioClip>("Audio/Looppi");
        intro = Resources.Load<AudioClip>("Audio/Intro");
        peace = Resources.Load<AudioClip>("Audio/peace");
        crowd = Resources.Load<AudioClip>("Audio/crowd");
     
    }

    private void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void PlayLoopMusic(AudioClip clip)
    {
        //Set the clip of our musicSource audio source to the clip passed in as a parameter.
        musicSource.clip = clip;

        //Play the clip.
        musicSource.loop = true;
        musicSource.Play();
    }
    public void PlayMusicOnce(AudioClip clip)
    {
        //Set the clip of our musicSource audio source to the clip passed in as a parameter.
        musicSource.clip = clip;

        //Play the clip.
        musicSource.Play();
    }

    //Used to play single sound clips from other sources than player.
    public void PlaySingle1(AudioClip clip)
    {
        if (clip != null)
        {
            if (audioSource1.loop == true)
            {
                audioSource1.loop = false;
            }
            //Set the clip of our efxSource audio source to the clip passed in as a parameter.
            audioSource1.clip = clip;

            //Play the clip.
            audioSource1.Play();
        }
    }

    //Used to play single sound clips.
    public void PlayLoop1(AudioClip clip)
    {
        //Set the clip of our efxSource audio source to the clip passed in as a parameter.
        audioSource1.clip = clip;

        //Play the clip.
        audioSource1.loop = true;
        audioSource1.Play();
    }

    //RandomizeSfx chooses randomly between various audio clips and slightly changes their pitch.
    public void RandomizeSfx1(AudioClip clip)
    {

        //Choose a random pitch to play back our clip at between our high and low pitch ranges.
        float randomPitch = Random.Range(lowPitchRange, highPitchRange);

        //Set the pitch of the audio source to the randomly chosen pitch.
        audioSource1.pitch = randomPitch;
        audioSource1.clip = clip;

        //Play the clip.
        audioSource1.Play();
    }

    public void PlaySingle2(AudioClip clip)
    {
        if (clip != null)
        {
            if (audioSource2.loop == true)
            {
                audioSource2.loop = false;
            }
            //Set the clip of our efxSource audio source to the clip passed in as a parameter.
            audioSource2.clip = clip;

            //Play the clip.
            audioSource2.Play();
        }
    }

    //Used to play single sound clips.
    public void PlayLoop2(AudioClip clip)
    {
        //Set the clip of our efxSource audio source to the clip passed in as a parameter.
        audioSource2.clip = clip;

        //Play the clip.
        audioSource2.loop = true;
        audioSource2.Play();
    }

    public void PlayLoop2RandomPitch(AudioClip clip)
    {
        float randomPitch = Random.Range(lowPitchRange, highPitchRange);

        //Set the clip of our efxSource audio source to the clip passed in as a parameter.
        audioSource2.clip = clip;
        audioSource2.pitch = randomPitch;

        //Play the clip.
        audioSource2.loop = true;
        audioSource2.Play();
    }

    //RandomizeSfx chooses randomly between various audio clips and slightly changes their pitch.
    public void RandomizeSfx(AudioClip clip)
    {

        //Choose a random pitch to play back our clip at between our high and low pitch ranges.
        float randomPitch = Random.Range(lowPitchRange, highPitchRange);

        //Set the pitch of the audio source to the randomly chosen pitch.
        audioSource2.pitch = randomPitch;
        audioSource2.clip = clip;

        //Play the clip.
        audioSource2.Play();
    }

    
    public void PlaySingle3(AudioClip clip)
    {
        if (clip != null)
        {
            if (audioSource3.loop == true)
            {
                audioSource3.loop = false;
            }
            //Set the clip of our efxSource audio source to the clip passed in as a parameter.
            audioSource3.clip = clip;

            //Play the clip.
            audioSource3.Play();
        }
    }

    //Used to play single sound clips.
    public void PlayLoop(AudioClip clip)
    {
        //Set the clip of our efxSource audio source to the clip passed in as a parameter.
        audioSource3.clip = clip;

        //Play the clip.
        audioSource3.loop = true;
        audioSource3.Play();
    }

    //RandomizeSfx chooses randomly between various audio clips and slightly changes their pitch.
    public void RandomizeSfx3(AudioClip clip)
    {

        //Choose a random pitch to play back our clip at between our high and low pitch ranges.
        float randomPitch = Random.Range(lowPitchRange, highPitchRange);

        //Set the pitch of the audio source to the randomly chosen pitch.
        audioSource3.pitch = randomPitch;
        audioSource3.clip = clip;

        //Play the clip.
        audioSource3.Play();
    }
    
}
