﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppearanceModel
{
    public Sprite hair;
    public Sprite brows;
    public Sprite eyes;
    public Sprite nose;
    public Sprite mouth;
    public Sprite body;

    public AppearanceModel(Sprite hair, Sprite brows, Sprite eyes, Sprite nose, Sprite mouth, Sprite body)
    {
        this.hair = hair;
        this.brows = brows;
        this.eyes = eyes;
        this.nose = nose;
        this.mouth = mouth;
        this.body = body;
    }

}
