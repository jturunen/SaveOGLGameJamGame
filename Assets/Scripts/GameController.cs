﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameController : MonoBehaviour
{
    public TextMeshProUGUI timer;
    public TextMeshProUGUI countDownText;
    public GameObject talkView;
    public Image hair, brows, eyes, nose, mouth, body;
    private TeamController teamController;
    private CharData character;

    private TalkModeScript talkModeScript;
    private MovementScript movementScript;
    public bool canStartTalk = true;

    public float targetTime = 60.0f;

    public float nextDeactivation = 0f;
    public float deactivationTime = 1.0f;
    public float deactivationMultiplier = 1.05f;

    public Slider timeSlider;

    public GameObject[] targetArray;

    public TextMeshProUGUI firstName, lastName;


    [TextArea]
    public List<string> firstNames;
    [TextArea]
    public List<string> lastNames;

    [TextArea]
    public List<string> jamTexts;

    internal bool gameStarted;

    // Start is called before the first frame update
    void Start()
    {
        teamController = GameObject.Find("GameController").GetComponent<TeamController>();
        talkModeScript = GameObject.Find("GameController").GetComponent<TalkModeScript>();
        movementScript = GameObject.Find("Player").GetComponent<MovementScript>();
        nextDeactivation = targetTime - deactivationTime;
        AudioManager.instance.PlayMusicOnce(AudioManager.instance.intro);

        timeSlider.maxValue = targetTime;
        talkView.SetActive(false);

        //Debug.Log("nextDeactivation: " + nextDeactivation);

        /*for (int i = 0; i < 1000; i++)
        {
            Debug.Log("hairPool + " + Random.Range(1, TargetSpriteController.Instance.hairPool.Length + 1));
            Debug.Log("browsPool + " + Random.Range(1, TargetSpriteController.Instance.browsPool.Length));
            Debug.Log("eyesPool + " + Random.Range(1, TargetSpriteController.Instance.eyesPool.Length));
            Debug.Log("nosePool + " + Random.Range(1, TargetSpriteController.Instance.nosePool.Length));
            Debug.Log("mouthPool + " + Random.Range(1, TargetSpriteController.Instance.mouthPool.Length));
            Debug.Log("bodyPool + " + Random.Range(1, TargetSpriteController.Instance.bodyPool.Length));
        }*/
        movementScript.canMove = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(!AudioManager.instance.musicSource.isPlaying)
        {
            AudioManager.instance.PlayLoopMusic(AudioManager.instance.medium);
            StartCoroutine(countDown());
        }

        if (talkView.activeInHierarchy)
        {
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                closeTalkMode();
                movementScript.canMove = true;
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                teamController.addToTeam(character);
                closeTalkMode();
                movementScript.canMove = true;
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
        }

        handleTimerTickDown();

    }

    private System.Collections.IEnumerator countDown()
    {
        gameStarted = true;
        movementScript.canMove = true;
        countDownText.text = getJam();
        countDownText.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.75f);
        countDownText.gameObject.SetActive(false);
    }

    private void handleTimerTickDown()
    {
        if (targetTime > 0)
        {
            targetTime -= Time.deltaTime;
            timeSlider.value += Time.deltaTime;

            timer.text = targetTime + "";

            if (targetTime <= nextDeactivation)
            {
                deactivateRandomTarget();
                nextDeactivation = nextDeactivation - deactivationTime * deactivationMultiplier;
                if (nextDeactivation < 0)
                {
                    nextDeactivation = 0;
                }
                //Debug.Log("nextDeactivation: " + nextDeactivation);
            }

            if (targetTime <= 0.0f)
            {
                targetTime = 0;
                //timer.text = targetTime + "";
                timerEnded();
            }
        }
    }

    private void deactivateRandomTarget()
    {
        int targetToDeactivate = UnityEngine.Random.Range(0, 100);

        for (int i = 0; i < targetArray.Length; i++)
        {
            if (i == targetToDeactivate)
            {
                //Debug.Log("targetToDeactivate: " + i);
                if (targetArray[i].GetComponent<TargetController>().recruitable)
                {
                    targetArray[i].GetComponent<TargetController>().deactivate();
                } else
                {
                    //Debug.Log("Was already deactivated: ");
                    // Was already deactivated, get a new random num;
                    targetToDeactivate = UnityEngine.Random.Range(0, 100);
                }
            }
        }
    }

    void timerEnded()
    {
        //do your stuff here.
        endGame();
    }

    public void endGame()
    {
        //Stop timer?
        //Get the list of team, and put it to player prefs to send it to next scene
        Debug.Log("endGame()");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void launchTalkMode(AppearanceModel appearance, CharData character)
    {
        setNames(getFirstName(), getLastName());
        movementScript.canMove = false;
        this.character = character;
        //Debug.Log("Launch talking! with " + character.role);
        talkView.SetActive(true);
        hair.sprite = appearance.hair;
        brows.sprite = appearance.brows;
        eyes.sprite = appearance.eyes;
        nose.sprite = appearance.nose;
        mouth.sprite = appearance.mouth;
        body.sprite = appearance.body;
        StartCoroutine(talkModeScript.startTalkTimer(this, character));
        AudioManager.instance.PlayLoop2RandomPitch(AudioManager.instance.speech);

    }

    public void closeTalkMode()
    {
        movementScript.canMove = true;
        AudioManager.instance.audioSource2.Stop();
        canStartTalk = true;
        talkView.SetActive(false);
        talkModeScript.resetUI();
    }

    public void setNames(string firstNameStr = "Pekka", string lastNameStr = "Pouta")
    {
        firstName.text = firstNameStr;
        lastName.text = lastNameStr;
    }

    public string getFirstName()
    {
        return firstNames[UnityEngine.Random.Range(0, firstNames.Count - 1)];
    }
    public string getLastName()
    {
        return lastNames[UnityEngine.Random.Range(0, firstNames.Count - 1)];
    }

    public string getJam()
    {
        return jamTexts[UnityEngine.Random.Range(0, jamTexts.Count - 1)];
    }

}
