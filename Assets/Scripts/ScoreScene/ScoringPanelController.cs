﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using System.Linq;

public class ScoringPanelController : MonoBehaviour
{
    public static int GOOD_REVIEW_SCORE = 40;
    public static int BAD_REVIEW_SCORE = 20;
    public static int STAR_TO_SCORE_MULTIPLIER = 4;
    public static int AUDIO_BONUS = 10;

    private string gameName = "";

    public TextMeshProUGUI titleReview;
    public TextMeshProUGUI genreBonusText;

    [TextArea]
    public List<string> reviewListGood;
    [TextArea]
    public List<string> reviewListMedium;
    [TextArea]
    public List<string> reviewListBad;

    public GameObject starObject;
    public GameObject emptyStarObject;

    CharData[] team;
    void Start()
    {
        gameName = PlayerPrefs.GetString(PlayerPrefKeys.CURRENT_GAME_NAME_KEY);

        team = new CharData[PlayerPrefs.GetInt(PlayerPrefKeys.NUMBER_OF_MEMBERS)];
        for (int i = 0; i < team.Length; i++)
        {
            string roleKey = "MEMBER_ROLE_" + i;
            string genreKey = "MEMBER_GENRE_" + i;
            string skillKey = "MEMBER_SKILL_" + i;
            CharData developer = new CharData(PlayerPrefs.GetInt(roleKey), PlayerPrefs.GetInt(genreKey), PlayerPrefs.GetInt(skillKey));
            team[i] = developer;
            //Debug.Log("Developer #" + i + " Role: " + developer.role + " Genre: " + developer.genre + " Skill: " + developer.skill);
        }

        handleReviewCalculations();
    }

    private void handleReviewCalculations()
    {
        int reviewScore = calculateReviewScore();
        PlayerPrefs.SetInt(PlayerPrefKeys.CURRENT_GAME_SCORE_KEY, reviewScore);

        if (reviewScore >= GOOD_REVIEW_SCORE)
        {
            titleReview.text = gameName + " " + getRandomReviewGood() + "\n" + reviewScore + " jammers loved it.";
        }
        else if (reviewScore <= BAD_REVIEW_SCORE)
        {
            titleReview.text = gameName + " " + getRandomReviewBad() + "\n" + reviewScore + " jammers loved it.";
        }
        else
        {
            titleReview.text = gameName + " " + getRandomReviewMedium() + "\n" + reviewScore + " jammers loved it.";
        }
    }

    private int calculateReviewScore()
    {
        float reviewScore = 0;
        int bestCode = 0;
        int bestArt = 0;
        int bestDesign = 0;

        for(int i = 0; i < team.Length; i++)
        {
            switch(team[i].role)
            {
                case AttributeLibrary.CODER:
                    bestCode = team[i].skill > bestCode ? team[i].skill : bestCode;
                    break;
                case AttributeLibrary.ARTIST:
                    bestArt = team[i].skill > bestArt ? team[i].skill : bestArt;
                    break;
                case AttributeLibrary.DESIGNER:
                    bestDesign = team[i].skill > bestDesign ? team[i].skill : bestDesign;
                    break;
                case AttributeLibrary.AUDIO:
                    reviewScore += AUDIO_BONUS;
                    break;
            }
        }
        showCodeStars(bestCode);
        showArtStars(bestArt);
        showDesignStars(bestDesign);

        reviewScore = convertStarsToScore(bestCode, bestArt, bestDesign);

        int[] arrayOfRepresentation = getArrayOfRepresentation();
        int highestRep = arrayOfRepresentation.Max();
        setGenreBonusText(highestRep);
        float multiplier = getGenreMultiplier(highestRep);
        Debug.Log("highestRep" + highestRep);
        Debug.Log("mult" + multiplier);
        reviewScore = reviewScore * multiplier;

        return (int) reviewScore;
    }

    private void setGenreBonusText(int genreBonus)
    {
        switch(genreBonus)
        {
            case 1:
                genreBonusText.text = "NO GENRE BONUS";
                break;
            case 2:
                genreBonusText.text = "GENRE BONUS";
                break;
            case 3:
                genreBonusText.text = "BIG GENRE BONUS!";
                break;
            case 4:
                genreBonusText.text = "GREAT GENRE BONUS!!";
                break;
            case 5:
                genreBonusText.text = "AMAZING GENRE BONUS!!!";
                break;
            default:
                genreBonusText.text = "NO GENRE BONUS";
                Debug.LogWarning("setGenreBonusText() defaulted");
                break;
        }
    }

    private void showCodeStars(int codeScore)
    {
        for(int i = 0; i < codeScore; i++)
        {
            GameObject gameObject = Instantiate(starObject);
            gameObject.transform.SetParent(transform);
            gameObject.transform.localPosition = new Vector2(-170 + (i * 120), 150);
            gameObject.SetActive(true);
        }
        int emptyStars = 5 - codeScore;
        for (int i = 0; i < emptyStars; i++)
        {
            GameObject gameObject = Instantiate(emptyStarObject);
            gameObject.transform.SetParent(transform);
            gameObject.transform.localPosition = new Vector2(-170 + (120 * codeScore) + (i * 120), 150);
            gameObject.SetActive(true);
        }
    }
    private void showArtStars(int artScore)
    {
        for (int i = 0; i < artScore; i++)
        {
            GameObject gameObject = Instantiate(starObject);
            gameObject.transform.SetParent(transform);
            gameObject.transform.localPosition = new Vector2(-170 + (i * 120), 0);
            gameObject.SetActive(true);
        }
        int emptyStars = 5 - artScore;
        for (int i = 0; i < emptyStars; i++)
        {
            GameObject gameObject = Instantiate(emptyStarObject);
            gameObject.transform.SetParent(transform);
            gameObject.transform.localPosition = new Vector2(-170 + (120 * artScore) + (i * 120), 0);
            gameObject.SetActive(true);
        }
    }
    private void showDesignStars(int designScore)
    {
        for (int i = 0; i < designScore; i++)
        {
            GameObject gameObject = Instantiate(starObject);
            gameObject.transform.SetParent(transform);
            gameObject.transform.localPosition = new Vector2(-170 + (i * 120), -150);
            gameObject.SetActive(true);
        }
        int emptyStars = 5 - designScore;
        for (int i = 0; i < emptyStars; i++)
        {
            GameObject gameObject = Instantiate(emptyStarObject);
            gameObject.transform.SetParent(transform);
            gameObject.transform.localPosition = new Vector2(-170 + (120 * designScore) + (i * 120), -150);
            gameObject.SetActive(true);
        }
    }

    private float getGenreMultiplier(int highestRep)
    {
        float multiplier = 1.0f;

        switch(highestRep)
        {
            case 1:
                multiplier = 0.5f;
                break;
            case 2:
                multiplier = 1.0f;
                break;
            case 3:
                multiplier = 1.3f;
                break;
            case 4:
                multiplier = 1.6f;
                break;
            case 5:
                multiplier = 2f;
                break;
            default:
                Debug.LogWarning("Defaulted genre multiplier");
                multiplier = 1.0f;
                break;

        }

        return multiplier;
    }

    private int[] getArrayOfRepresentation()
    {
        int rpgRep = 0;
        int actionRep = 0;
        int strategyRep = 0;
        int puzzleRep = 0;
        int adultRep = 0;

        for (int i = 0; i < team.Length; i++)
        {
            switch (team[i].genre)
            {
                case AttributeLibrary.RPG:
                    rpgRep++;
                    break;
                case AttributeLibrary.ACTION:
                    actionRep++;
                    break;
                case AttributeLibrary.STRATEGY:
                    strategyRep++;
                    break;
                case AttributeLibrary.PUZZLE:
                    puzzleRep++;
                    break;
                case AttributeLibrary.ADULT:
                    adultRep++;
                    break;
            }
        }

        int[] array = new int[5];

        array[0] = rpgRep;
        array[1] = actionRep;
        array[2] = strategyRep;
        array[3] = puzzleRep;
        array[4] = adultRep;

        return array;
    }

    private int convertStarsToScore(int code, int art, int design)
    {
        int score = (code * STAR_TO_SCORE_MULTIPLIER) + (art * STAR_TO_SCORE_MULTIPLIER) + (design * STAR_TO_SCORE_MULTIPLIER);
        return score;
    }

    private string getRandomReviewGood()
    {
        return reviewListGood[UnityEngine.Random.Range(0, reviewListGood.Count)];
    }

    private string getRandomReviewBad()
    {
        return reviewListBad[UnityEngine.Random.Range(0, reviewListBad.Count)];
    }
    private string getRandomReviewMedium()
    {
        return reviewListMedium[UnityEngine.Random.Range(0, reviewListMedium.Count)];
    }
}